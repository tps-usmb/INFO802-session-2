import axios from "axios"
import { reduce } from "lodash"
import React, { useEffect, useState } from "react"
import { Product } from "../common/products"


export default function App() {

    const [productsLoading, setProductsLoading] = useState(false)
    const [products, setProducts] = useState<Product[] | undefined>()

    // Récupérer les produits au chargement de l'app
    useEffect(() => {
        async function fetch() {
            setProducts(undefined)
            setProductsLoading(true)

            try {
                const { data } = await axios.get("/api/products")
                setProducts(data.products)
            }
            catch (e) {
                console.error("Cannot fetch products:", e)
            }
            finally {
                setProductsLoading(false)
            }
        }

        fetch()
    }, [])


    const [cart, setCart] = useState<Product[]>([])

    const [shippingCost, setShippingCost] = useState<number | undefined>()
    const [shippingCostLoading, setShippingCostLoading] = useState(false)
    const [cost, setCost] = useState<number | undefined>()

    // Récupérer les coûts de livraison depuis l'API
    async function getShippingCost() {
        setShippingCost(undefined)
        setShippingCostLoading(true)

        try {
            const { data } = await axios.post("/api/shipping", { cart })
            setShippingCost(data.shippingCost)
        }
        catch (e) {
            console.error("Cannot fetch shipping cost:", e)
        }
        finally {
            setShippingCostLoading(false)
        }
    }

    // Mettre à jour le prix total quand les coûts de livraison sont modifiés
    useEffect(() => {
        if (shippingCost) {
            setCost(
                reduce(
                    cart,
                    (acc, product) => acc + product.price,
                    0,
                ) + shippingCost
            )
        }
        else
            setCost(undefined)
    }, [shippingCost])

    // Réinitialiser les coûts de livraison (et le prix total) quand le panier est modifié
    useEffect(() => {
        setShippingCost(undefined)
    }, [cart])


    return <>
        <h2>Tous les produits</h2>

        <div>
            {productsLoading ?
                <p>Chargement...</p>
            :
                products ?
                    products.map(product =>
                        <React.Fragment key={product.ref}>
                            <h5>{product.ref}</h5>
                            <p>Prix : {product.price} € / Poids : {product.weight} g</p>
                            <button onClick={() => setCart([...cart, product])}>Ajouter au panier</button>
                        </React.Fragment>
                    )
                :
                    <p>Impossible de charger les produits.</p>
            }
        </div>

        <h2>Panier</h2>

        <div>
            {cart.map((product, key) =>
                <React.Fragment key={key}>
                    <h5>{product.ref}</h5>
                    <p>Prix : {product.price} € / Poids : {product.weight} g</p>
                    <button onClick={() => setCart([...cart.slice(0, key), ...cart.slice(key + 1)])}>Supprimer</button>
                </React.Fragment>
            )}
        </div>

        <div>
            <p>
                <button
                    onClick={() => {
                        if (cart.length > 0)
                            getShippingCost()
                        else
                            alert("Veuillez ajouter au moins un article dans le panier !")
                    }}

                    disabled={shippingCostLoading}
                >
                    {shippingCostLoading ? "Chargement..." : "Calculer le total"}
                </button>&nbsp;&nbsp;

                {cost && shippingCost &&
                    <span>Total : {cost.toFixed(2)} € (dont {shippingCost.toFixed(2)} € de frais de livraison)</span>
                }
            </p>
        </div>
    </>

}
