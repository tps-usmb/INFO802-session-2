import express from "express"
import { resolve } from "path"
import { api } from "./api"


(async () => {
    const app = express()

    app.use("/api", api)

    // En mode développement, on proxy les requêtes vers le JS vers Vite pour build le front
    if (process.env.NODE_ENV === "development") {
        const { createProxyMiddleware } = await import("http-proxy-middleware")

        const proxy = createProxyMiddleware({
            target: "http://127.0.0.1:3000/",
            changeOrigin: true,
        })

        app.use("/", proxy)
    }
    // En mode production, on sert les fichiers statiques générés
    else {
        app.use("/assets", express.static("dist/assets"))

        app.get("/*", (_, res) => {
            res.sendFile(resolve(process.cwd(), "dist", "index.html"), () => { res.end() })
        })
    }


    const port = process.env.PORT || 8080
    app.listen(port, () => console.log("Server running on port " + port + "!"))
})()
