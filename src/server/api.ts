import { json, Router } from "express"
import { every, map, reduce } from "lodash"
import { Product } from "../common/products"
import { products, productsByRef } from "./products"
import { shippingCostPerG } from "./shipping"


export const api = Router()
api.use(json())

api.get("/products", (_, res) => {
    res.json({ products })
})

api.post("/shipping", (req, res) => {
    // On vérifie qu'une liste de produits a bien été donnée
    if (!req.body.cart) {
        res.status(422).json({ error: "Produits manquants." })
        return
    }

    // On vérifie que la référence de tous les produits existe bien
    if (!every(
        req.body.cart as Product[],

        product => Boolean(
            productsByRef[ product.ref ]
        ),
    )) {
        res.status(422).json({ error: "Les références de certains produits sont incorrectes." })
        return
    }

    // On ne fait pas confiance aux données de l'utilisateur : on mappe les données du serveur sur les références données
    const cart = map(
        req.body.cart as Product[],
        product => productsByRef[ product.ref ],
    )

    // Calcul des frais de livraison
    res.json({
        shippingCost: reduce(
            cart,
            (acc, product) => acc + product.weight * shippingCostPerG,
            0,
        ),
    })
})
