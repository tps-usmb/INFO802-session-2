import { groupBy, mapValues } from "lodash"
import { Product } from "../common/products"


export const products: Product[] = [
    {
        ref: "Poulet braisé 1kg",
        price: 15.00,
        weight: 1000,
    },

    {
        ref: "Coca Cola 50cl",
        price: 0.80,
        weight: 100,
    },

    {
        ref: "2 Steaks Hachés Eco+ 300g",
        price: 3.00,
        weight: 300,
    },

    {
        ref: "Boîte de 10 chewings gum Mulholland Drive",
        price: 4.50,
        weight: 50,
    },
]

export const productsByRef = mapValues(
    groupBy(products, product => product.ref),
    products => products[0],
)
