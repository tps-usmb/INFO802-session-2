export type Product = {
    ref: string
    price: number
    weight: number
}
